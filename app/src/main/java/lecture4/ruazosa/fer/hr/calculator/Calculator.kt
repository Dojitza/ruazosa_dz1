package lecture4.ruazosa.fer.hr.calculator

import android.util.Log
import android.widget.Toast

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    var expression_clean: MutableList<String> = mutableListOf()
    private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
        expression_clean = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)

            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {

        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        result = expression[0].toDouble()
        var i=0
        var skip=0
        while(i < expression.count()- 1) {
            skip=0
            if (expression[i+1]=="*" || expression[i+1]=="/"){
                var temp_result=expression[i].toDouble()
                var j=i
                var end=false
                while(j < expression.count()-2 && !end){
                    when (expression[j+1]){
                        "*" -> temp_result *= expression[j+2].toDouble()
                        "/" -> temp_result /= expression[j+2].toDouble()
                        "+" -> end=true
                        "-" -> end=true
                    }
                    j++;
                    skip++
                }
                expression_clean.add(temp_result.toString())
            }
            else{
                expression_clean.add(expression[i])
            }
            if (i+1 == expression.count()-1){
                expression_clean.add(expression[i+1])
            }
            Log.i("skip",skip.toString())

            if (skip>0){
                i+=skip
            }
            else
                i++

            Log.i("gradnja", expression_clean.toString())

        }


        expression=expression_clean
        Log.i("clanovi_liste", expression.toString())

        result=expression[0].toDouble()
        for(i in 1..expression.count()- 1 step 2) {
            when(expression[i]) {
                "+" -> result = result + expression[i+1].toDouble()
                "-" -> result = result - expression[i+1].toDouble()
            }
        }
    }
}